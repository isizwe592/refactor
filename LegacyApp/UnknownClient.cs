﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegacyApp
{
    public class UnknownClient : IClient
    {
        User user;

        public UnknownClient(User user)
        {
            this.user = user;
        }

        public bool HasCreditLimit()
        {
            // Do credit check
            user.HasCreditLimit = true;
            using (var userCreditService = new UserCreditServiceClient())
            {
                var creditLimit = userCreditService.GetCreditLimit(user.Firstname, user.Surname, user.DateOfBirth);
                user.CreditLimit = creditLimit;
            }

            return true;
        }
    }
}

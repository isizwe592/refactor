﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegacyApp
{
    public static class UserObjectFactory
    {
        public static IClient Create(string className, User user)
        {
            if (className == "VeryImportantClient")
            {
                return new VeryImportantClient(user);
            }
            else if (className == "ImportantClient")
            {
                return new ImportantClient(user);
            }
            else
            {
                return new UnknownClient(user);
            }
        }
    }
}

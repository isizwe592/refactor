﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegacyApp
{
    public class VeryImportantClient : IClient
    {
        User user;
        public VeryImportantClient(User user)
        {
            this.user = user;
        }

        public bool HasCreditLimit()
        {
            user.HasCreditLimit = false;

            return false;
        }
    }
}

﻿using System;

namespace LegacyApp
{
    public class UserService
    {
        public bool AddUser(string firname, string surname, string email, DateTime dateOfBirth, int clientId)
        {
            ValidateUser.ValidNameAndSurname(firname, surname);

            ValidateUser.ValidEmail(email);

            ValidateUser.ValidAge(dateOfBirth);

            var clientRepository = new ClientRepository();
            var client = clientRepository.GetById(clientId);

            var user = new User
                               {
                                   Client = client,
                                   DateOfBirth = dateOfBirth,
                                   EmailAddress = email,
                                   Firstname = firname,
                                   Surname = surname
                               };

            var objClient = UserObjectFactory.Create(client.Name, user);
            objClient.HasCreditLimit();

            ValidateUser.ValidCreditLimit(user);

            UserDataAccess.AddUser(user);

            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegacyApp
{
    public class ImportantClient : IClient
    {
        User user;

        public ImportantClient(User user)
        {
            this.user = user;
        }

        public bool HasCreditLimit()
        {
            // Do credit check and double credit limit
            user.HasCreditLimit = true;
            using (var userCreditService = new UserCreditServiceClient())
            {
                var creditLimit = userCreditService.GetCreditLimit(user.Firstname, user.Surname, user.DateOfBirth);
                creditLimit = creditLimit * 2;
                user.CreditLimit = creditLimit;
            }

            return true;
        }
    }
}

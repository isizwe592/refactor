﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegacyApp
{
    public abstract class ValidateUser
    {
        public static bool ValidNameAndSurname(string firname, string surname) => (string.IsNullOrEmpty(firname) || string.IsNullOrEmpty(surname));

        public static bool ValidEmail(string email) => (!email.Contains("@") && !email.Contains("."));

        public static bool ValidAge(DateTime dateOfBirth) {
            int age = ValidOfBirth(dateOfBirth);
            if (age< 21)
            {
                return false;
            }
            return true;
        }

        private static int ValidOfBirth(DateTime dateOfBirth)
        {
            var now = DateTime.Now;
            int age = now.Year - dateOfBirth.Year;
            if (now.Month < dateOfBirth.Month || (now.Month == dateOfBirth.Month && now.Day < dateOfBirth.Day)) return age--;

            return age;
        }

        public static bool ValidCreditLimit(User user) => user.HasCreditLimit && user.CreditLimit < 500;
    }
}
